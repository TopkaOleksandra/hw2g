//получаем имя папки проекта

import * as nodePath from 'path'
const rootFolder = nodePath.basename(nodePath.resolve());


//путь к папке с результатом, будет создаваться автоматически в процессе
const buildFolder=`./dist`;

//путь к папке с исходниками
const srcFolder=`./src`;

export const path={
    //объект путей к папке с результатом
    build: {
        js: `${buildFolder}/js/`,
        css:`${buildFolder}/css/`,
        html: `${buildFolder}/`,
        files:`${buildFolder}/files/`,
        images: `${buildFolder}/img/`,
        fonts: `${buildFolder}/fonts/`,},
        
        //объект путей с исходным файлом
    src: {
        js: `${srcFolder}/js/app.js`,
        scss: `${srcFolder}/scss/style.scss`,
        html: `${srcFolder}/*.html`,
        files:`${srcFolder}/files/**/*.*`,
        images: `${srcFolder}/img/**/*.{jpg,jpeg,png,gif,webp}`,
        svg: `${srcFolder}/img/**/*.svg`,},
        
    //файлы за которыми должен следить gulp
    watch: {
        js: `${srcFolder}/js/**/*.js`,
        scss: `${srcFolder}/scss/**/*.scss`,
        html:`${srcFolder}/**/*.html`,
        files: `${srcFolder}/files/**/*.*`,
        images: `${srcFolder}/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}`,},
        
        
    clean: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,}