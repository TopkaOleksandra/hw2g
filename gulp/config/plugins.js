import replace from "gulp-replace";//пошук та заміна
import plumber from 'gulp-plumber';//обробка помилок
import notify from 'gulp-notify';//сообщения подсказки
import browsersync from 'browser-sync'//локальний сервер
import newer from 'gulp-newer'//проверка обновления
import ifPlugin from "gulp-if"//разделение режима разработчика

export const plugins = {
    replace: replace,
    plumber: plumber,
    notify: notify,
    browsersync: browsersync,
    newer: newer,
    if: ifPlugin
}